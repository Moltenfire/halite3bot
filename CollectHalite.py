from MatchRunner import MatchRunner, SIZES, Player
import argparse

class Stats(object):
    def __init__(self, size="", total_games=0, points=0, halite_collected=0, halite_available=0, collisions=0, terminated=0):
        self.size = size
        self.total_games = total_games
        self.points = points
        self.halite_collected = halite_collected
        self.halite_available = halite_available
        self.collisions = collisions
        self.terminated = terminated

    def __iadd__(self, other):
        self.total_games += other.total_games
        self.points += other.points
        self.halite_collected += other.halite_collected
        self.halite_available += other.halite_available
        self.collisions += other.collisions
        self.terminated += other.terminated
        return self

    def __repr__(self):
        if self.total_games:
            return "{} - {} {:.1f} {:5.2f}% C: {} T: {} P: {}".format(self.size, self.halite_collected, self.halite_collected / self.total_games, self.halite_collected / self.halite_available * 100, self.collisions, self.terminated, self.points)
        else:
            return "No games"

    def logline(self):
        print(self)

    @classmethod
    def from_results(cls, results, size, player_id=0):
        points = sum([r.points(player_id) for r in results])
        halite_collected = sum(result.score(player_id) for result in results)
        halite_available = sum(result.total_halite() for result in results)
        collisions = sum(result.collisions(player_id) for result in results)
        terminated = sum(result.terminated(player_id) for result in results)
        return cls(size, len(results), points, halite_collected, halite_available, collisions, terminated)

def run_match(total_games, size, p0_args=None, seed=0):
    mr = MatchRunner.two_player_match(size, Player.default_para(p0_args), Player.still())
    results = mr.match(n=total_games, seed_val=seed)
    stats = Stats.from_results(results, size)
    stats.logline()
    return stats

def run_all_sizes(total_games):
    all_stats = Stats("A")
    for size in SIZES:
        stats = run_match(total_games, size)
        all_stats += stats
        
    all_stats.logline()

def main(total_games, size, seed):
    if size is not None:
        run_match(total_games, size, seed=seed)
    else:
        run_all_sizes(total_games)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--games', '-g', action="store", default=5, type=int)
    parser.add_argument('--size', '-s', action="store", default=None, type=int)
    parser.add_argument('--seed', '-d', action="store", default=0, type=int)
    args = parser.parse_args()
    main(total_games=args.games, size=args.size, seed=args.seed)
