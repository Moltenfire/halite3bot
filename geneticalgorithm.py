import argparse
from GA import GA
from MatchRunner import Player, MatchRunner
from CollectHalite import Stats
import numpy as np
import time
import os
import logging

TURNS = {
    32 : 401,
    40 : 426,
    48 : 451,
    56 : 476,
    64 : 501,
}

class LargestSum(GA):

    def cal_pop_fitness(self, population, directory):
        fitness = np.zeros(population.shape[0])
        for i in range(population.shape[0]):
            fitness[i] = np.sum(population[i,:])

        idx = np.argmax(fitness)
        print("Best Sum {} Params {}".format(fitness[idx], list(population[idx])))
        return fitness

class TournamentGA(GA):

    def __init__(self, size, games, num_weights=15, population_size=6, num_parents_mating=3):
        GA.__init__(self, num_weights, population_size, num_parents_mating)
        self.size = size
        self.games = games
        self.max_turn = TURNS[size]
        self.seed = 0
        self.weights = np.array([
            self.max_turn, 100, 1, 1000, 100,
            self.size, self.max_turn, self.size, self.size,
            self.size, 5, 5, 5, 1000, 4])
        
        logging.basicConfig(
            level=logging.INFO,
            format="%(message)s",
            handlers=[
                logging.FileHandler(os.path.join(self.directory, "full_log.log")),
                logging.StreamHandler()
            ])

    def get_initial_population(self):
        population = GA.get_initial_population(self)
        population[0, :] = np.array([223, 10, 0, 850, 40, 24, 100, 8, 12, 10, 1, 4, 0, 200, 1]) / self.weights
        population[1, :] = np.array([220, 50, 0.1000539960989426, 850, 18, 28, 100, 8, 20, 10, 1, 4, 0, 200, 1]) / self.weights
        population[2, :] = np.array([213, 60, 0.1000539960989426, 850, 12, 28, 100, 8, 20, 10, 1, 4, 0, 200, 1]) / self.weights

        fitness = np.array(list(reversed(range(population.shape[0]))))
        parents = self.select_mating_pool(population, fitness)
        offspring_crossover = self.crossover(parents)
        offspring_mutation = self.mutation(offspring_crossover)

        new_population = np.zeros((self.population_size, self.num_weights))
        new_population[0:parents.shape[0], :] = parents
        new_population[parents.shape[0]:, :] = offspring_mutation

        return new_population

    def cal_pop_fitness(self, population, directory):
            self.seed = self.get_seed()
            start = time.time()

            players = []
            logging.info("--- Players ---")
            for i in range(population.shape[0]):
                para = self.get_bot_parameters(population[i,:])
                logging.info("{} {}".format(i, para))
                player = Player.default_para(para)
                players.append(player)

            fitness = np.zeros(population.shape[0])
            for i, player in enumerate(players):
                logging.info("--- Player {} ---".format(i))
                mr = MatchRunner(self.size, os.path.join(directory, "{}".format(i)))
                mr.add_player(player)
                mr.add_player(Player.enemy(2))
                mr.add_player(Player.enemy(0))
                mr.add_player(Player.enemy(0))
                results = mr.match(n=self.games, seed_val=self.seed)
                all_stats = [Stats.from_results(results, self.size, j) for j in range(results[0].total_players())]
                for stats in all_stats:
                    logging.info(stats)
                    # stats.logline()
                halite_collected = all_stats[0].halite_collected
                enemy_halite_collected = max(stats.halite_collected for stats in all_stats[1:])
                fitness[i] = halite_collected - enemy_halite_collected

            logging.info("--- Scores ---")
            indices = np.flip(np.argsort(fitness))
            for idx in indices:
                logging.info("Player {} Score {} ".format(idx, fitness[idx]))

            end = time.time()
            total_time = end - start
            logging.info("Total time {:.1f}".format(total_time))
            logging.info("Avg time {:.2f}".format(total_time / self.population_size))

            return fitness

    def get_bot_parameters(self, chromosome):
        params = list((chromosome * self.weights).astype(float))
        floats = set([2, 14])
        params = [int(p) if i not in floats else p for i, p in enumerate(params)]
        return params

    def get_seed(self):
        return np.random.randint(10000000)

def main(size, games):
    ga = TournamentGA(size, games)
    ga.run(1000)

    # /cygdrive/c/Users/molte/Documents/Python/Halite3
    # python geneticalgorithm.py -g 4 -s 48

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', '-s', action="store", default=32, type=int)
    parser.add_argument('--games', '-g', action="store", default=5, type=int)
    args = parser.parse_args()
    main(args.size, args.games)