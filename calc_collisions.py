from bot.collision import fix_collisions
# data = [(2, 0, False, (7, 27), (7, 27)), (5, 1, False, (14, 30), (14, 30)), (8, 1, False, (25, 8), (25, 8)), (12, 1, False, (24, 11), (24, 11)), (16, 1, False, (22, 8), (22, 8)), (17, 1, False, (1, 6), (1, 6)), (31, 1, False, (25, 6), (25, 6)), (38, 1, False, (2, 4), (2, 4)), (44, 1, False, (26, 8), (26, 8)), (23, 2, False, (21, 30), (21, 29)), (24, 2, False, (24, 7), (24, 6)), (29, 2, False, (20, 27), (20, 26)), (1, 2, False, (21, 25), (21, 24)), (3, 2, False, (28, 17), (28, 18)), (4, 2, False, (23, 24), (24, 24)), (6, 2, False, (29, 2), (29, 1)), (7, 2, False, (29, 1), (29, 0)), (9, 2, False, (2, 28), (2, 27)), (10, 2, False, (29, 30), (29, 29)), (11, 2, False, (28, 1), (28, 0)), (13, 2, False, (28, 15), (28, 16)), (14, 2, False, (8, 5), (8, 6)), (15, 2, False, (3, 29), (2, 29)), (18, 2, False, (23, 27), (23, 26)), (19, 2, False, (8, 16), (8, 16)), (20, 2, False, (29, 3), (29, 2)), (21, 2, False, (11, 14), (10, 14)), (22, 2, False, (28, 16), (28, 17)), (25, 2, False, (28, 25), (29, 25)), (26, 2, False, (29, 0), (29, 31)), (27, 2, False, (2, 14), (3, 14)), (28, 2, False, (21, 26), (21, 25)), (30, 2, False, (22, 24), (23, 24)), (32, 2, False, (30, 3), (29, 3)), (33, 2, False, (12, 8), (11, 8)), (35, 2, False, (28, 28), (28, 27)), (36, 2, False, (9, 18), (9, 17)), (37, 2, False, (11, 13), (11, 14)), (39, 2, False, (7, 14), (7, 15)), (40, 2, False, (28, 19), (28, 20)), (41, 2, True, (8, 17), (8, 16)), (42, 2, False, (28, 30), (28, 29)), (43, 2, False, (29, 24), (29, 24)), (34, 4, False, (27, 9), (28, 9))]
# data = [(25, 0, False, (30, 8), (30, 8)), (47, 0, False, (15, 23), (15, 23)), (1, 1, False, (28, 13), (28, 13)), (3, 1, False, (28, 23), (28, 23)), (4, 1, False, (31, 17), (31, 17)), (6, 1, False, (24, 18), (24, 18)), (7, 1, False, (22, 20), (22, 20)), (9, 1, False, (3, 26), (3, 26)), (11, 1, False, (28, 14), (28, 14)), (15, 1, False, (5, 10), (5, 10)), (16, 1, False, (28, 25), (28, 25)), (17, 1, False, (25, 15), (25, 15)), (19, 1, False, (26, 14), (26, 14)), (21, 1, False, (1, 21), (1, 21)), (22, 1, False, (26, 24), (26, 24)), (23, 1, False, (25, 22), (25, 22)), (27, 1, False, (4, 11), (4, 11)), (30, 1, False, (14, 25), (14, 25)), (31, 1, False, (30, 12), (30, 12)), (32, 1, False, (24, 14), (24, 14)), (33, 1, False, (14, 14), (14, 14)), (35, 1, False, (14, 19), (14, 19)), (36, 1, False, (26, 8), (26, 8)), (37, 1, False, (27, 6), (27, 6)), (38, 1, False, (23, 17), (23, 17)), (39, 1, False, (2, 12), (2, 12)), (40, 1, False, (13, 22), (13, 22)), (41, 1, False, (8, 26), (8, 26)), (42, 1, False, (11, 27), (11, 27)), (43, 1, False, (14, 4), (14, 4)), (45, 1, False, (12, 1), (12, 1)), (5, 2, False, (27, 22), (27, 21)), (10, 2, False, (25, 20), (26, 20)), (12, 2, False, (26, 20), (27, 20)), (28, 2, False, (27, 19), (27, 20)), (29, 2, False, (9, 9), (9, 10)), (0, 4, False, (28, 20), (27, 20)), (2, 4, False, (29, 20), (28, 20)), (8, 4, False, (27, 20), (26, 20)), (13, 4, False, (23, 19), (23, 18)), (18, 4, False, (2, 18), (2, 19)), (20, 4, False, (2, 13), (1, 13)), (24, 4, False, (7, 27), (6, 27)), (26, 4, False, (31, 15), (30, 15)), (34, 4, False, (4, 21), (4, 22)), (44, 4, False, (11, 18), (11, 19)), (46, 4, False, (26, 15), (25, 15)), (48, 4, False, (13, 23), (14, 23)), (49, 4, False, (0, 15), (31, 15)), (50, 4, False, (8, 17), (9, 17)), (51, 4, False, (8, 16), (8, 17))]
# data = [(3, 0, False, (3, 0), (3, 0)), (6, 0, False, (11, 2), (11, 2)), (18, 0, False, (8, 10), (8, 10)), (0, 1, False, (29, 14), (29, 14)), (5, 1, False, (3, 5), (3, 5)), (8, 1, False, (3, 2), (3, 2)), (16, 1, False, (31, 3), (31, 3)), (7, 2, False, (4, 1), (3, 1)), (9, 2, False, (8, 14), (8, 15)), (10, 2, False, (4, 2), (4, 1)), (15, 2, False, (4, 3), (4, 2)), (1, 4, False, (2, 1), (3, 1)), (2, 4, False, (8, 9), (8, 8)), (4, 4, False, (3, 1), (2, 1)), (12, 4, False, (3, 8), (3, 7)), (13, 4, False, (9, 2), (10, 2)), (14, 4, False, (11, 12), (12, 12)), (17, 4, False, (17, 16), (18, 16)), (19, 4, False, (8, 11), (8, 10)), (20, 4, False, (8, 16), (8, 15))]
# data = [(1, 0, False, (9, 16), (9, 16)), (0, 1, False, (8, 17), (8, 17)), (2, 4, False, (8, 16), (9, 16))]
# data = [(0, 1, False, (6, 23), (6, 23)), (3, 1, False, (9, 13), (9, 13)), (5, 1, False, (13, 19), (13, 19)), (6, 1, False, (3, 17), (3, 17)), (7, 1, False, (11, 16), (11, 16)), (2, 2, False, (8, 12), (8, 13)), (4, 2, False, (8, 13), (8, 14)), (1, 4, False, (8, 15), (8, 14)), (8, 4, False, (8, 22), (8, 23)), (9, 4, False, (8, 16), (7, 16))]
data = [(0, 1, False, (6, 23), (6, 23)), (3, 1, False, (9, 13), (9, 13)), (5, 1, False, (13, 19), (13, 19)), (6, 1, False, (3, 17), (3, 17)), (7, 1, False, (11, 16), (11, 16)), (8, 1, False, (8, 23), (8, 23)), (2, 2, False, (8, 13), (8, 14)), (4, 2, False, (8, 14), (8, 15)), (1, 4, False, (8, 15), (8, 14)), (9, 4, False, (7, 16), (7, 17))]

        # ship_pos = [(
        #     ship_move.ship_id,
        #     ship_move.priority,
        #     # self.ship_info[ship_move.ship_id].state == ShipState.RETURN_END and self.distance_to_dropoff(self.ship_info[ship_move.ship_id].ship.position) <= 1,
        #     self.ship_info[ship_move.ship_id].state == ShipState.RETURN_END and self.game_map.calculate_distance(self.ship_info[ship_move.ship_id].ship.position, self.ship_info[ship_move.ship_id].target) <= 1,
        #     (ship_move.start.x, ship_move.start.y),
        #     (ship_move.end.x, ship_move.end.y)) for ship_move in ship_moves]
        # logging.info("all_end_pos {}".format(ship_pos))

class Move():
    def __init__(self, ship_id, priority, no_collision_check, start, end):
        self.ship_id = ship_id
        self.priority = priority
        self.no_collision_check = no_collision_check
        self.start = start
        self.end = end
        self.direction = None

    def __repr__(self):
        return "{} {} {} {} {}".format(self.ship_id, self.priority, self.no_collision_check, self.start, self.end)

    def __lt__(self, other):
        return (self.priority, self.ship_id) < (other.priority, self.ship_id)

    def __hash__(self):
        return hash(self.ship_id)

moves = []
for i, p, r, s, e in data:
    moves.append(Move(i, p, r, s, e))

moves.sort()

for m in moves:
    print(m)

end_pos = [move.end for move in moves]
print(len(end_pos) - len(set(end_pos)))

fix_collisions(moves)

end_pos = [move.end for move in moves]
print(len(end_pos) - len(set(end_pos)))

for m in moves:
    print(m)