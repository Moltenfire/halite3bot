import numpy as np
from scipy import ndimage

a = np.zeros((10,10))

b = np.resize(np.arange(100, dtype=float), (10,10))

a[4,4] = 1
a[5,6] = 1

k = np.array(
    [[0,0,0,0,1,0,0,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,1,1,1,1,1,0,0],
     [0,1,1,1,1,1,1,1,0],
     [1,1,1,1,1,1,1,1,1],
     [0,1,1,1,1,1,1,1,0],
     [0,0,1,1,1,1,1,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,0,0,1,0,0,0,0]]
)

r = ndimage.convolve(a, k, mode='wrap')
# print(a)
# print(k)

r[r < 2] = 1
r[r > 1] = 2

print(r)
print(b)

b *= r

print(b)
