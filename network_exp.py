import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time

def main():
    halite_map = np.load("halite_map.npy")
    size = halite_map.shape[0]
    x, y = 8, 16
    # print(halite_map[x,y])

    G = nx.DiGraph()

    for x in range(size):
        for y in range(size):
            p = (x,y)
            v = halite_map[x,y]

            surrounding = [
                (((x - 1) % size, y), 'W'),
                (((x + 1) % size, y), 'E'),
                ((x, (y - 1) % size), 'N'),
                ((x, (y + 1) % size), 'S')
            ]

            for q, d in surrounding:
                G.add_edge(p, q, weight=v, direction=d)

    path = nx.dijkstra_path(G, (0,0), (2,2))
    print(path)
    print(nx.dijkstra_path_length(G, (0,0), (2,2)))

    # edgesinpath=zip(path[0:],path[1:])
    # for u,v in edgesinpath:
    #     print(G[u][v]['direction'])

    # res = nx.single_source_dijkstra_path_length(G, (8,16))
    # print(res)



start = time.time()
main()
end = time.time()
print(end - start)
