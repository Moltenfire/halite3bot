from MatchRunner import Player, MatchRunner, SIZES
from CollectHalite import Stats
import numpy as np
import argparse
from itertools import product

np.random.seed(0)

def duel(size, games, progress, player, four_players):
    print("---", player)

    if four_players:
        enemies = [Player.enemy(2), Player.enemy(0), Player.enemy(0)]
    else:
        enemies = [Player.enemy(2)]
    
    mr = MatchRunner(size)
    mr.add_player(player)
    for e in enemies:
        mr.add_player(e)

    results = mr.match(n=games, seed_val=6, progress=progress)

    all_stats = [Stats.from_results(results, size, j) for j in range(results[0].total_players())]
    for stats in all_stats:
        stats.logline()
    halite_collected = all_stats[0].halite_collected
    enemy_halite_collected = max(stats.halite_collected for stats in all_stats[1:])
    fitness = (halite_collected - enemy_halite_collected) / games

    print(fitness)

    return fitness

def duels(size, games, progress, four_players, players):
    print(size, games, four_players)
    for player in players:
        print(player)

    res = []
    for p in players:
        res.append(duel(size, games, progress, p, four_players))

    for i, player in enumerate(players):
        print(player, res[i])

def main(size, all_sizes, games, progress, four_players):

    # p0 = Player.default()
    # p0 = Player.from_bot_parameters(4 if four_players else 2, size)
    p0 = Player.default_para([162, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1, 80])
    p1 = Player.default_para([162, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1, 100])
    p2 = Player.default_para([162, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1, 120])
    p2 = Player.default_para([162, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1, 140])
    # p1 = Player.default_para([158, 60, 0.10, 898, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1])
    # p2 = Player.default_para([158, 60, 0.10, 915, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1])
    # p3 = Player.default_para([158, 60, 0.10, 912, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1])


    players = []
    # players.append(p0)
    # players.append(p1)
    players.append(p2)
    # players.append(p3)
    # players.append(p4)

    if all_sizes:
        for s in SIZES:
            duel(s, games, progress, p, four_players)
    else:
        duels(size, games, progress, four_players, players)


def other():

    # print("Match 40 4")
    # players = []
    # return_amount = [850, 870, 890, 910]
    # dropoff_min_halite = [200, 250, 300]
    # for ra, dmh in product(return_amount, dropoff_min_halite):
    #     players.append(Player.default_para([220, 60, 0.10, ra, 18, 28, 100, 8, 20, 10, 1, 4, 0, dmh, 1]))
    # duels(40, 12, False, True, players)

    # print("Match 32 4 dropoff")
    # dropoff_min_time = [80, 100, 120]
    # dropoff_min_halite = [200, 300, 400]
    # players = []
    # players.append(Player.default_para([158, 60, 0.10, 900, 65, 25, 100, 6, 12, 10, 0, 4, 0, 200, 1]))
    # for dmt, dmh in product(dropoff_min_time, dropoff_min_halite):
    #     players.append(Player.default_para([158, 60, 0.10, 900, 65, 25, dmt, 6, 12, 10, 1, 4, 0, dmh, 1]))
    # duels(32, 12, False, True, players)

    # print("Match 64 4")
    # players = []
    # players.append(Player.default_para([220, 60, 0.10, 830, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1])) # 171053
    # # players.append(Player.default_para([220, 60, 0.10, 850, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1])) # 258463
    # # players.append(Player.default_para([220, 60, 0.10, 870, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1])) # 166012
    # players.append(Player.default_para([220, 60, 0.10, 890, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1])) # -33240
    # players.append(Player.default_para([220, 60, 0.10, 910, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1])) # -85867
    # duels(64, 8, False, True, players)

    # print("Match 32 4")
    # players = []
    # players.append(Player.default_para([162, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # players.append(Player.default_para([155, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # players.append(Player.default_para([158, 60, 0.10, 900, 45, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # players.append(Player.default_para([158, 60, 0.10, 900, 25, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # duels(32, 10, False, True, players)

    # print("Match 32 4")
    # players = []
    # players.append(Player.default_para([158, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # players.append(Player.default_para([180, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # players.append(Player.default_para([200, 60, 0.10, 900, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1]))
    # duels(32, 10, False, True, players)

    # players = []
    # players.append(Player.default_para([273, 60, 0.10, 850, 40, 28, 100, 8, 12, 10, 1, 4, 0, 200, 1, 0]))
    # players.append(Player.default_para([273, 60, 0.10, 850, 40, 28, 100, 8, 12, 10, 1, 4, 0, 200, 1, 60]))
    # players.append(Player.default_para([273, 60, 0.10, 850, 40, 28, 100, 8, 12, 10, 1, 4, 0, 200, 1, 100]))
    # duels(56, 4, False, False, players)
    
    players = []
    max_build_time = [220, 240, 260]
    enemy_move_cost = [20, 60, 100]
    halite_max_dist = [16, 20, 24]
    for x,y,z in product(max_build_time, enemy_move_cost, halite_max_dist):
        players.append(Player.default_para([x, 60, 0.10, 850, 12, 28, 100, 8, z, 10, 2, 4, 0, 200, 1, y]))
    duels(64, 4, False, True, players)

    players = []
    max_build_time = [220, 240, 260]
    enemy_move_cost = [20, 60, 100, 140]
    halite_max_dist = [16, 20, 24]
    for x,y,z in product(max_build_time, enemy_move_cost, halite_max_dist):
        players.append(Player.default_para([x, 60, 0.10, 850, 12, 24, 100, 8, z, 10, 1, 4, 0, 200, 1, y]))
    duels(48, 4, False, True, players)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', '-s', action="store", default=32, type=int)
    parser.add_argument('--games', '-g', action="store", default=10, type=int)
    parser.add_argument('--progress', '-p', action='store_true')
    parser.add_argument('--four', '-4', action='store_true')
    parser.add_argument('--all_sizes', '-a', action='store_true')
    args = parser.parse_args()
    # main(args.size, args.all_sizes, args.games, args.progress, args.four)
    other()
