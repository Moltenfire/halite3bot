from tournament import Tournament
import numpy as np
import argparse
from copy import deepcopy
import datetime

np.random.seed(0)

values0 = {
    32 : [230, 10, 850, 40, 15, 0],
    40 : [244, 10, 850, 40, 20, 0],
    48 : [258, 10, 850, 40, 25, 0],
    56 : [273, 10, 850, 40, 30, 0],
    64 : [287, 10, 850, 40, 35, 0]
}

values1 = {
    32 : [230, 10, 850, 40, 15, 0],
    40 : [244, 10, 850, 40, 20, 0],
    48 : [258, 10, 850, 40, 25, 0],
    56 : [273, 10, 850, 40, 30, 0],
    64 : [287, 10, 850, 40, 35, 0]
}

values2 = {
    32 : [230, 10, 850, 40, 15, 0],
    40 : [244, 10, 850, 40, 20, 0],
    48 : [258, 10, 850, 40, 25, 0],
    56 : [273, 10, 850, 40, 30, 0],
    64 : [287, 10, 850, 40, 35, 0]
}

values3 = {
    32 : [230, 10, 850, 40, 15, 0],
    40 : [244, 10, 850, 40, 20, 0],
    48 : [258, 10, 850, 40, 25, 0],
    56 : [273, 10, 850, 40, 30, 0],
    64 : [287, 10, 850, 40, 35, 0]
}

def main(size, players, games):

    d = datetime.datetime.now().strftime("duels/duel_%Y%m%d-%H-%M-%S")

    p0 = values0[size]
    p1 = values1[size]
    para = [p0, p1]
    if players == 4:
        p2 = values2[size]
        p3 = values3[size]
        para.append(p2)
        para.append(p3)

    t = Tournament(para, size, dir=d, games=games)
    t.run(players)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', '-s', action="store", default=32, type=int)
    parser.add_argument('--players', '-p', action="store", default=2, type=int)
    parser.add_argument('--games', '-g', action="store", default=10, type=int)
    args = parser.parse_args()
    main(args.size, args.players, args.games)
