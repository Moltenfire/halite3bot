import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time
from hlt.positionals import Direction, Position
from collections import defaultdict
from copy import deepcopy
from math import ceil, floor

class Ship():
    def __init__(self, pos, cargo=0):
        self.pos = pos
        self.cargo = cargo

class State():
    def __init__(self, halite_map):
        self.halite_map = halite_map
        self.ships = []

    def perform_moves(self, moves, spawn=None):
        halite_map = copy(self.halite_map)
        ships = copy(self.ships)

        for i, move in enumerate(moves):
            if move == Direction.Still:
                amount = ceil(self.halite_map[ships[i].pos] * 0.25)
                self.halite_map[ships[i].pos] -= amount
                ships[i].cargo += amount
            else:
                ships[i].pos = ships[i].pos.directional_offset(move)


class Sim():
    def __init__(self, n, halite_map, spawn_turns):
        self.n = n
        self.halite_map = halite_map
        self.spawn_turns = spawn_turns
        self.spawn_pos = Position(n, n, False)
        self.possible_moves = {}
        for i in range(halite_map.shape[0]):
            for j in range(halite_map.shape[1]):
                m = []
                if i > 0:
                    m.append(Direction.West)
                if i < (halite_map.shape[0] - 1):
                    m.append(Direction.East)
                if j > 0:
                    m.append(Direction.North)
                if j < (halite_map.shape[1] - 1):
                    m.append(Direction.South)
                self.possible_moves[Position(i,j, False)] = m

    def run(self, ships=[], collected=0, turn_number=0):
        moves = []
        for ship in ships:
            m = [Direction.Still]
            if ship.cargo >= 0.1 * halite_map[ship.pos.x, ship.pos.y]:
                m.extend(self.possible_moves[ship.pos])
            moves.append(m)

        if turn_number in self.spawn_turns:
            # if 
            ships.append(Ship(Position((halite_map.shape[0] - 1) / 2)))

        

def main(halite_map, n=1):
    
    size = halite_map.shape[0]
    x, y = 8, 16
    
    submap = halite_map[x-n:x+n+1, y-n:y+n+1]

    spawn_turns = set([0])
    ships = []

    s = Sim(n, submap, spawn_turns)
    s.run()


halite_map = np.load("halite_map.npy")
start = time.time()
main(halite_map)
end = time.time()
print(end - start)
