#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt

# This library contains constant values.
from hlt import constants

# This library contains direction metadata to better interface with the game.
from hlt.positionals import Direction, Position

# This library allows you to generate random numbers.
import random

# Logging allows you to save messages for yourself. This is required because the regular STDOUT
#   (print statements) are reserved for the engine-bot communication.
import logging

import numpy as np

""" <<<Game Begin>>> """

# This game object contains the initial game state.
game = hlt.Game()
# At this point "game" variable is populated with initial map data.
# This is a good place to do computationally expensive start-up pre-processing.
# As soon as you call "ready" function below, the 2 second per turn timer will start.

SHIP_HALITE_RETURN = 850
MAX_SHIPS = 3

START = 0
MOVE = 1
COLLECTING = 2
RETURN = 3
# ship_state = MOVE
# target = None
done_positions = set()

state_priority = {
    COLLECTING : 0,
    RETURN : 1,
    MOVE : 2,
    START : 3
}

class ShipInfo():
    def __init__(self, id):
        self.id = id
        self.target = None
        self.state = START

    def priority(self, can_move):
        return (state_priority[self.state], can_move, self.id)

ship_info = {}
game_map = game.game_map
halite_map = np.zeros((game_map.width, game_map.height))
ship_pos = set()

game.ready("MyPythonBot")

# Now that your bot is initialized, save a message to yourself in the log file with some important information.
#   Here, you log here your id, which you can always fetch from the game object by using my_id.
logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))

""" <<<Game Loop>>> """

def get_distances(game_map, p):
    distances = np.ones((game_map.width, game_map.height))
    for x in range(distances.shape[0]):
        for y in range(distances.shape[1]):
            pos = Position(x,y)
            d = game_map.calculate_distance(p, pos)
            distances[x,y] = max(1, d)
    return distances

def get_cost_to_move(halite_map, p):
    return int(halite_map[p.x, p.y] / 10)

def can_ship_move(halite_map, ship):
    cost = get_cost_to_move(halite_map, ship.position)
    halite = ship.halite_amount
    return cost <= halite

while True:
    # This loop handles each turn of the game. The game object changes every turn, and you refresh that state by
    #   running update_frame().
    game.update_frame()
    # You extract player metadata and the updated map metadata here for convenience.
    me = game.me
    game_map = game.game_map

    # A command queue holds all the commands you will run this turn. You build this list up and submit it at the
    #   end of the turn.
    command_queue = []

    # Update Ship info
    ships = me.get_ships()
    ship_ids = set([s.id for s in ships])
    new_ship_ids = ship_ids - set(ship_info.keys())
    del_ship_ids = set(ship_info.keys()) - ship_ids
    logging.info(ship_ids)
    if new_ship_ids:
        logging.info("New ships {}".format(new_ship_ids))
    if del_ship_ids:
        logging.info("Removed ships {}".format(del_ship_ids))
    for ship_id in new_ship_ids:
        ship_info[ship_id] = ShipInfo(ship_id)
    for ship_id in del_ship_ids:
        ship_info.pop(ship_id)

    for x in range(game_map.width):
        for y in range(game_map.height):
            pos = Position(x,y)
            halite_map[x,y] = game_map[pos].halite_amount

    next_ship_pos = set()

    ships.sort(key=lambda x : x.id)

    for ship in ships:
        logging.info("Ship {} at {} - H:{} S:{} HoM:{} Can Move:{}".format(ship.id, ship.position, ship.halite_amount, ship_info[ship.id].state, game_map[ship.position].halite_amount, can_ship_move(halite_map, ship)))

    # Check if objective completed
    logging.info("--- Completed actions ---")
    for ship in ships:
        start_state = ship_info[ship.id].state
        if start_state == START:
            logging.info("Ship {} starting. Choosing new target".format(ship.id, ship.position))
            ship_info[ship.id].state = MOVE
        if start_state == MOVE:
            if ship.position == ship_info[ship.id].target:
                logging.info("Ship {} reached target {} started collecting".format(ship.id, ship.position))
                ship_info[ship.id].state = COLLECTING
        if start_state == COLLECTING:
            halite_remaining = game_map[ship.position].halite_amount
            if halite_remaining < 10 or ship.halite_amount > SHIP_HALITE_RETURN:
                if ship.halite_amount > SHIP_HALITE_RETURN:
                    logging.info("Ship {} finished collecting. Returning to base".format(ship.id))
                    ship_info[ship.id].state = RETURN
                else:
                    logging.info("Ship {} finished collecting. Choosing new target".format(ship.id))
                    ship_info[ship.id].target = None
                    ship_info[ship.id].state = MOVE
        if start_state == RETURN:
            if ship.position == me.shipyard.position:
                logging.info("Ship {} dropped off cargo. Choosing new target".format(ship.id))
                ship_info[ship.id].state = MOVE
                ship_info[ship.id].target = None

    logging.info("--- Calculate next actions ---")
    for ship in sorted(ships, key=lambda x :[ship_info[x.id].priority(can_ship_move(halite_map, x))]):
        # For each of your ships, move randomly if the ship is on a low halite location or the ship is full.
        #   Else, collect halite.
        move = None
        start_state = ship_info[ship.id].state
        if start_state == MOVE:
            if ship_info[ship.id].target is None:
                distances = get_distances(game_map, ship.position)
                scores = halite_map / distances
                i,j = np.unravel_index(scores.argmax(), scores.shape)
                ship_info[ship.id].target = Position(i, j)
                done_positions.add(ship_info[ship.id].target)
                logging.info("Ship {} targeting {}".format(ship.id, ship_info[ship.id].target))

            if can_ship_move(halite_map, ship):
                res = game_map.get_unsafe_moves(ship.position, ship_info[ship.id].target)
                move = res[0]
                next_pos = ship.position.directional_offset(move)
                logging.info("Ship {} moving to {}".format(ship.id, next_pos))
            else:
                logging.info("Ship {} stuck at {}".format(ship.id, ship.position))
        elif start_state == COLLECTING:
            logging.info("Ship {} collecting at {}".format(ship.id, ship.position))
            next_pos = ship.position
        elif start_state == RETURN:
            if can_ship_move(halite_map, ship):
                logging.info("Ship {} return to base at {}".format(ship.id, me.shipyard.position))
                move = game_map.get_unsafe_moves(ship.position, me.shipyard.position)[0]
                next_pos = ship.position.directional_offset(move)

        if move is not None:
            p = (next_pos.x, next_pos.y)
            if p not in next_ship_pos:
                command_queue.append(ship.move(move))
                next_ship_pos.add(p)
        else:
            p = (ship.position.x, ship.position.y)
            if p in next_ship_pos:
                logging.error("Collision going to happen at {}".format(p))
            next_ship_pos.add(p)

    # for p in sorted(list(next_ship_pos)):
        # logging.info("Next position {}".format(p))

    # If the game is in the first 200 turns and you have enough halite, spawn a ship.
    # Don't spawn a ship if you currently have a ship at port, though - the ships will collide.
    if game.turn_number <= 200 and len(ships) < MAX_SHIPS:
        if me.halite_amount >= constants.SHIP_COST and not game_map[me.shipyard].is_occupied:
            logging.info("Spawning ship")
            command_queue.append(me.shipyard.spawn())

    # Send your moves back to the game environment, ending this turn.
    game.end_turn(command_queue)

