from MatchRunner import MatchRunner, Player
import argparse

def run_game(size, seed, p0, p1, p0_args=None, p1_args=None):
    m = MatchRunner.two_player_match(size, Player(p0, p0_args), Player(p1, p1_args))
    res = m.game(seed=seed)
    print("Winner {2} - {0}x{0} ({1})".format(res.size(), res.seed(), res.winner()))
    for i in range(2):
        t = "terminated" if res.terminated(i) else ""
        print("P{} {} {} {}".format(i, res.score(i), res.collisions(i), t))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', '-s', action="store", default=None)
    parser.add_argument('--seed', '-d', action="store", default=None)
    parser.add_argument('--p0', action="store", default="MyBot.py")
    parser.add_argument('--p1', action="store", default="MyBot.py")
    args = parser.parse_args()
    run_game(size=args.size, seed=args.seed, p0=args.p0, p1=args.p1)
