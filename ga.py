import numpy as np
import datetime
import os
import logging

class GA(object):

    def __init__(self, num_weights=7, population_size=8, num_parents_mating=4):
        self.num_weights = num_weights
        self.population_size = population_size
        self.num_parents_mating = num_parents_mating

        self.directory = datetime.datetime.now().strftime("tournaments/tournament_%Y%m%d-%H-%M-%S")
        os.makedirs(self.directory)

    def run(self, num_generations=3):
        population = self.get_initial_population()

        for i in range(num_generations):
            population = self.run_generation(i, population)

    def get_initial_population(self):
        population = np.random.uniform(size=(self.population_size, self.num_weights))
        population = np.clip(population, 0, 1)
        return population

    def run_generation(self, gen_num, population):
        logging.info("--- Generation {} ---".format(gen_num))
        directory = "{}/G{:03}".format(self.directory, gen_num)
        os.makedirs(directory)

        fitness = self.cal_pop_fitness(population, directory)

        new_population = self.get_new_population(population, fitness)

        # Save state
        self.save_state(new_population, directory)

        return new_population

    def cal_pop_fitness(self, population, directory):
        fitness = np.zeros(population.shape[0])
        print("Best {}".format(list(population[np.argmax(fitness)])))
        return fitness

    def get_new_population(self, population, fitness):
        parents = self.select_mating_pool(population, fitness)
        offspring_crossover = self.crossover(parents)
        offspring_mutation = self.mutation(offspring_crossover)

        new_population = np.zeros((self.population_size, self.num_weights))
        new_population[0:parents.shape[0], :] = parents
        new_population[parents.shape[0]:, :] = offspring_mutation

        return new_population

    def select_mating_pool(self, population, fitness):
        parents = np.empty((self.num_parents_mating, population.shape[1]))
        largest_idx = (-fitness).argsort()[:self.num_parents_mating]
        for i, idx in enumerate(largest_idx):
            parents[i, :] = population[idx, :]
        return parents

    def crossover(self, parents, indpb=0.5):
        offspring_size = self.population_size - self.num_parents_mating
        offspring = np.empty((offspring_size, parents.shape[1]))

        for k in range(offspring_size):
            p1_idx, p2_idx = np.random.choice(self.num_parents_mating, 2, replace=False)

            for i in range(parents.shape[1]):
                p = p1_idx if np.random.random_sample() < indpb else p2_idx
                offspring[k, i] = parents[p, i]

        return offspring

    def mutation(self, offspring_crossover, mutation_rate=0.2):
        for i in range(offspring_crossover.shape[0]):
            for j in range(offspring_crossover.shape[1]):
                if np.random.random_sample() < mutation_rate:
                    new_val = offspring_crossover[i,j] + (0.1 * np.random.randn())
                    offspring_crossover[i,j] = new_val

        return np.clip(offspring_crossover, 0, 1)

    def save_state(self, population, directory):
        np.save(os.path.join(directory, "population.npy"), population)
        random_state = np.random.get_state()
        with open(os.path.join(directory, "random_state.txt"), 'w') as f:
            f.writelines(str(random_state))
