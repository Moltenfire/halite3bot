#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt

# This library contains constant values.
from hlt import constants

from bot.gamecontroller import GameController

# Logging allows you to save messages for yourself. This is required because the regular STDOUT
#   (print statements) are reserved for the engine-bot communication.
import logging
import time
import sys
import numpy as np

""" <<<Game Begin>>> """

def main():

    # This game object contains the initial game state.
    game = hlt.Game()
    # At this point "game" variable is populated with initial map data.
    # This is a good place to do computationally expensive start-up pre-processing.
    # As soon as you call "ready" function below, the 2 second per turn timer will start.

    gc = GameController.from_game(game, sys.argv)

    game.ready("Moltenfire")

    # Now that your bot is initialized, save a message to yourself in the log file with some important information.
    #   Here, you log here your id, which you can always fetch from the game object by using my_id.
    logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))

    """ <<<Game Loop>>> """

    turn_times = []

    while True:
        game.update_frame()

        start = time.time()

        command_queue = gc.get_commands(game)

        end = time.time()
        turn_times.append(end - start)

        if game.turn_number == constants.MAX_TURNS:
            logging.info("Total turn time {:.4f}".format(np.sum(turn_times)))
            logging.info("Max turn time {:.4f}".format(np.max(turn_times)))
            logging.info("Avg turn time {:.4f}".format(np.mean(turn_times)))

            gc.game_state.stats.log()

        game.end_turn(command_queue)
    
    

if __name__ == '__main__':
    main()
