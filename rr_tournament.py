from collections import defaultdict
from itertools import combinations
import numpy as np
from CollectHalite import Stats
from MatchRunner import MatchRunner

class Tournament(object):

    def __init__(self, players, size, dir="tornament", games=4):
        self.players = players
        self.size = size
        self.dir = dir
        self.games = games
        self.results = defaultdict(Stats)

    def total_players(self):
        return len(self.players)

    def run(self, players=2):
        return self.run2() if players == 2 else self.run4()

    def run2(self):
        for player_ids in combinations(range(self.total_players()), 2):
            a, b = player_ids
            print("{} vs {}".format(a, b))
            mr = MatchRunner(size=self.size, replays=self.dir + "/{}_{}".format(a, b))
            for i in range(2):
                mr.add_player(self.players[player_ids[i]])

            results = mr.match(n=self.games, seed_val=np.random.randint(1000000000))
            s0 = Stats.from_results(results, self.size, 0)
            s1 = Stats.from_results(results, self.size, 1)
            s0.logline()
            s1.logline()
            self.results[a] += s0
            self.results[b] += s1

        print("--- Results ---")
        out = []
        max_points = (self.total_players() - 1) * self.games
        player_ids = list(range(self.total_players()))
        player_ids.sort(key=lambda i : self.results[i].points, reverse=True)
        for i in player_ids:
            points = self.results[i].points
            print("{} {} {} {:5.2f}%".format(i, "#", points, points / max_points * 100 )) # self.para[i]

    def run4(self):
        for player_ids in combinations(range(self.total_players()), 4):
            a, b, c, d = player_ids
            print("{} vs {} vs {} vs {}".format(a, b, c, d))
            mr = MatchRunner(size=self.size, replays=self.dir + "/{}_{}_{}_{}".format(a,b,c,d))
            for i in range(4):
                mr.add_player(args=self.parastr(player_ids[i]))

            results = mr.match(n=self.games, seed_val=np.random.randint(1000000000))
            for i in range(4):
                s = Stats.from_results(results, self.size, i)
                s.logline()
                self.results[player_ids[i]] += s

        print("--- Results ---")
        out = []
        max_points = (self.players - 1) * self.games
        player_ids = list(range(self.players))
        player_ids.sort(key=lambda i : self.results[i].points, reverse=True)
        for i in player_ids:
            points = self.results[i].points
            print("{} {} {} {:5.2f}%".format(i, self.para[i], points, points / max_points * 100 ))

        return np.array([self.results[i].points for i in range(self.players)])
