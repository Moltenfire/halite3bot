import subprocess
import json
from random import sample, seed
from collections import defaultdict
from pathlib import Path
from multiprocessing.pool import ThreadPool
import os
import re
import tqdm
from bot.parameters import values

SEED_VAL = 0
TOTAL_POOLS = 4
SIZES = [32, 40, 48, 56, 64]
TURNS = {
    32 : 401,
    40 : 426,
    48 : 451,
    56 : 476,
    64 : 501,
}

class Player():
    def __init__(self, file, para=None):
        self.file = file
        self.para = para

    def cmd(self):
        return '"python '+ self._python_cmd() + '"'

    def _python_cmd(self):
        cmd = self.file
        if self.para is not None:
            cmd += ' ' + self.parastr()
        return cmd

    def __repr__(self):
        return self._python_cmd()

    def parastr(self):
        return ','.join(list(map(str, self.para)))

    @classmethod
    def default(cls):
        return cls("MyBot.py")

    @classmethod
    def default_para(cls, para):
        return cls("MyBot.py", para)

    @classmethod
    def from_bot_parameters(cls, players, size):
        return cls("MyBot.py", values[players][size][::])

    @classmethod
    def still(cls):
        return cls("StillBot.py")

    @classmethod
    def enemy(cls, i):
        enemies = [
            "other/zalmar/MyBot.py",
            "other/louislung/5_.py",
            "other/louislung/6_.py"
        ]
        return cls(enemies[i])

class Result():
    def __init__(self, res):
        self.res = res

    @classmethod
    def from_cmd(cls, output):
        return cls(json.loads(output))

    @classmethod
    def from_file(cls, filepath):
        with open(filepath) as f:
            return cls(json.loads(f.read()))

    def seed(self):
        return self.res['map_seed']

    def size(self):
        return self.res['map_width']

    def total_players(self):
        return len(self.res['stats'])

    def score(self, player_id):
        return self.res['stats'][str(player_id)]['score']

    def terminated(self, player_id):
        t = self.res['terminated']
        if str(player_id) in t:
            return t[str(player_id)]
        return False

    def winner(self):
        for k, v in self.res['stats'].items():
            if v['rank'] == 1:
                return int(k)

    def points(self, player_id):
        players = len(self.res['stats'])
        return players - self.res['stats'][str(player_id)]['rank']

    def total_halite(self):
        return self.res['map_total_halite']

    def total_turns(self):
        return TURNS[self.size()]

    def error_log(self, player_id):
        error_logs = self.res['error_logs']
        pid = str(player_id)
        if pid in error_logs:
            return Path(error_logs[pid])

    def collisions(self, player_id):
        error_file = self.error_log(player_id)
        c = 0
        before = self.total_turns() - 25
        if error_file is not None and error_file.is_file():
            with open(error_file, 'r') as f:
                for line in f.readlines():
                    if "Turn " == line[:5]:
                        turn = int(line[5:])
                    if "collided on cell" in line:
                        if turn < before:
                            total_numbers = len(list(map(int, re.findall("\d+", line))))
                            c += total_numbers - 2
        return c

class MatchRunner():

    def __init__(self, size=None, replays="replays/", use_threads=True):
        self.players = []
        self.size = size
        self.replays = replays
        self.use_threads = use_threads
        if not os.path.exists(self.replays):
            os.makedirs(self.replays)

    @classmethod
    def two_player_match(cls, size, p0, p1):
        m = cls(size=size)
        m.add_player(p0)
        m.add_player(p1)
        return m

    def add_player(self, player=Player.default()):
        self.players.append(player)

    def get_pool(self):
        pc = TOTAL_POOLS
        # if len(self.players) == 4:
        #     pc /= 2
        return ThreadPool(int(pc))

    def match(self, n=25, seed_val=SEED_VAL, progress=False):
        seed(seed_val)
        seeds = sample(range(1000000), n)

        if self.use_threads:
            pool = self.get_pool()
            res = pool.map(self.game, seeds)
            pool.close()
        else:
            res = [self.game(s) for s in tqdm.tqdm(seeds, disable=(not progress))]

        return res

    def get_player_cmd(self, player_id):
        return self.players[player_id].cmd()

    def game(self, seed=None):
        args = "halite.exe --replay-directory {} -v --results-as-json".format(self.replays).split()
        for i in range(len(self.players)):
            args.append(self.get_player_cmd(i))
        if seed is not None:
            args.extend("-s {}".format(seed).split())
        if self.size is not None:
            args.extend(" --width {0} --height {0}".format(self.size).split())
        # print(' '.join(args))
        output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
        res = Result.from_cmd(output)

        return res

def main():
    total_results = defaultdict(int)
    for size in SIZES:
        mr = MatchRunner(size=size)
        results = mr.match()
        winners = [r.winner() for r in results]
        p0 = winners.count(0)
        p1 = winners.count(1)
        total_results[0] += p0 
        total_results[1] += p1 
        print("{}x{} {}-{} {:4.1f}%".format(size, size, p0, p1, p0/(p0+p1)*100))

    p0, p1 = total_results[0], total_results[1]
    print("All {}-{} {:4.1f}%".format(p0, p1, p0/(p0+p1)*100))

if __name__ == '__main__':
    main()
