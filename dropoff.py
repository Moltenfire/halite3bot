import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time
from scipy import ndimage

np.set_printoptions(threshold=np.inf)

k = np.array(
    [[0,1,0],
     [1,1,1],
     [0,1,0]]
)

def main():
    halite_map = np.load("halite_map.npy")
    size = halite_map.shape[0]
    home = (8, 16)

    scores = ndimage.convolve(halite_map, k, mode='wrap')

    ind = np.unravel_index(np.argsort(scores, axis=None), scores.shape)
    coords = list(reversed(list(zip(ind[0], ind[1]))))
    
    for x, y in coords[:5]:
        print(x, y, halite_map[x,y], scores[x,y])





start = time.time()
main()
end = time.time()
print(end - start)
