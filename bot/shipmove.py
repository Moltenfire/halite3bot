from bot.shipinfo import ShipState

class ShipMove():
    def __init__(self, ship_id, start, direction, priority, no_collision_check):
        self.ship_id = ship_id
        self.start = start
        self.no_collision_check = no_collision_check
        self.set_new_direction(direction)
        self.priority = priority

    def set_new_direction(self, direction):
        self.direction = direction
        self.end = self.start.directional_offset(direction)

    def __repr__(self):
        return "{} {} {} {} {} {}".format(self.ship_id, self.direction, self.priority, self.no_collision_check, self.start, self.end)

    @classmethod
    def from_si(cls, si, direction, distance_to_target):
        return cls(si.id, si.ship.position, direction, si.priority(), si.state == ShipState.RETURN_END and distance_to_target <= 1)
