import logging
import numpy as np

class Stats():
    
    def __init__(self, game):
        self.game = game
        self.ship_started = {}
        self.completed_runs = []
    
    def ship_started_run(self, i):
        self.ship_started[i] = self.game.turn_number

    def ship_ended_run(self, i):
        total_turns = self.game.turn_number - self.ship_started[i]
        self.completed_runs.append(total_turns)

    def log(self):
        logging.info("Total completed runs {}".format(len(self.completed_runs)))
        logging.info("Avg. turns per run {:.2f}".format(np.mean(self.completed_runs)))

        n = 5
        running_avg = np.convolve(self.completed_runs, np.ones((n,))/n, mode='valid')
        logging.info("Turns per run {}".format(self.completed_runs))
        logging.info("Running Avg. {}".format(running_avg))
