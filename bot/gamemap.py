import numpy as np
import networkx as nx
import logging
from bot.constants import MOVE_ADDITIONAL_COST
from bot.iterutils import pairwise
from hlt.positionals import Position

class GameMap(object):

    def __init__(self, game_map, parameters):
        self.game_map = game_map
        self.parameters = parameters
        self.enemy_ships = []
        self.halite_map = np.zeros((self.size(), self.size()), dtype=int)
        self.halite_graph = self._get_halite_graph()

    def update(self, game_map, enemy_ships):
        self.game_map = game_map
        self.enemy_ships = enemy_ships
        for x in range(self.size()):
            for y in range(self.size()):
                self.halite_map[x,y] = self.halite_amount(Position(x,y))
        self.halite_graph = self._get_halite_graph()

    def size(self):
        return self.game_map.width

    def halite_amount(self, pos):
        return self.game_map[pos].halite_amount

    def calculate_distance(self, p, q):
        return self.game_map.calculate_distance(p, q)

    def cost_to_move(self, position):
        return int(self.halite_amount(position) / 10)

    def get_distances(self, position):
        distances = np.zeros((self.size(), self.size()), dtype=int)
        res = nx.single_source_shortest_path_length(self.halite_graph, (position.x, position.y))
        for x in range(distances.shape[0]):
            for y in range(distances.shape[1]):
                d = res[(x,y)]
                distances[x,y] = max(1, d)
        return distances

    def get_path(self, p, q):
        G = self.halite_graph
        path = nx.dijkstra_path(G, (p.x,p.y), (q.x,q.y))
        return [G[u][v]['direction'] for u,v in pairwise(path)]

    def get_path_cost(self, p, q):
        return nx.dijkstra_path_length(self.halite_graph, (p.x,p.y), (q.x,q.y))

    def _get_halite_graph(self):
        enemy_ship_positions = set([(ship.position.x, ship.position.y) for ship in self.enemy_ships])

        G = nx.DiGraph()
        size = self.size()
        for x in range(size):
            for y in range(size):
                p = (x,y)
                v = self.halite_map[x,y] + self.parameters.move_additional_cost

                surrounding = [
                    (((x - 1) % size, y), (-1, 0)),
                    (((x + 1) % size, y), (1, 0)),
                    ((x, (y - 1) % size), (0, -1)),
                    ((x, (y + 1) % size), (0, 1))
                ]

                for q, d in surrounding:
                    w = v
                    if q in enemy_ship_positions:
                        w += self.parameters.enemy_ship_avoid_cost
                    G.add_edge(p, q, weight=v, direction=d)

        return G
