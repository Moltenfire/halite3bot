import logging
from bot.shipinfo import ShipInfo, ShipState
from bot.shipmove import ShipMove
from bot.gamemap import GameMap
from bot.collision import fix_collisions
from bot.constants import *
from bot.stats import Stats
from hlt.positionals import Direction, Position
from hlt import constants

import numpy as np
from scipy import ndimage
from collections import Counter, defaultdict
from itertools import permutations
import random
random.seed(0)

INSPIRATION_CONV = np.array(
    [[0,0,0,0,1,0,0,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,1,1,1,1,1,0,0],
     [0,1,1,1,1,1,1,1,0],
     [1,1,1,1,1,1,1,1,1],
     [0,1,1,1,1,1,1,1,0],
     [0,0,1,1,1,1,1,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,0,0,1,0,0,0,0]])

DROPOFF_CONV = np.array(
    [[0,0,1,0,0],
     [0,1,1,1,0],
     [1,1,1,1,1],
     [0,1,1,1,0],
     [0,0,1,0,0]]
)
DROPOFF_CONV_SUM = np.sum(DROPOFF_CONV)

CONSTRUCTION_DROPOFF_CONV = np.array(
    [[0,0,0,0,1,0,0,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,1,1,1,1,1,0,0],
     [0,1,1,1,1,1,1,1,0],
     [1,1,1,1,1,1,1,1,1],
     [0,1,1,1,1,1,1,1,0],
     [0,0,1,1,1,1,1,0,0],
     [0,0,0,1,1,1,0,0,0],
     [0,0,0,0,1,0,0,0,0]])

class GameState(object):

    def __init__(self, game, parameters):
        self.game = game
        self.parameters = parameters
        self.me = game.me
        self.game_map = GameMap(game.game_map, parameters)
        self.ships = []
        self.ship_info = {}

        self.stats = Stats(game)

        self.early_return = True

    def update(self, game):
        self.game = game
        self.me = game.me
        self.game_map.update(game.game_map, self.get_enemy_ships())
        self.ships = self.me.get_ships()
        self.update_ship_info()

    def update_completed_actions(self):
        if self.turns_remaining() == self.parameters.end_return_time:
            enemy_positions = self.get_enemy_base_positions()
            for si in self.ship_info.values():
                d = self.distance_to_dropoff(si.ship.position)
                if d <= self.parameters.end_return_time:
                    logging.info("Ship {} set to return at end d {} h {}".format(si.id, d, si.ship.halite_amount))
                    si.state = ShipState.RETURN_END
                    si.target = self.nearest_dropoff(si.ship.position)
                else:
                    distances = {p : self.game_map.calculate_distance(si.ship.position, p) for p in enemy_positions}
                    res = min(distances, key=lambda x : distances[x])
                    si.state = ShipState.BLOCKADE
                    si.blockade_target = res

        state_shipids = defaultdict(list)
        for si in self.ship_info.values():
            state_shipids[si.state].append(si)

        ships_require_targets = []

        # Start
        for si in state_shipids[ShipState.START]:
            logging.info("Ship {} starting. Choosing new target".format(si.id, si.ship.position))
            si.state = ShipState.MOVE
            ships_require_targets.append(si)

        # Move
        for si in state_shipids[ShipState.MOVE]:
            if si.ship.position == si.target:
                logging.info("Ship {} reached target {} started collecting".format(si.id, si.ship.position))
                si.state = ShipState.COLLECTING

        # Collecting
        for si in state_shipids[ShipState.COLLECTING]:
            halite_remaining = self.game_map.halite_amount(si.ship.position)
            if si.ship.halite_amount > self.parameters.ship_return_dropoff:
                logging.info("Ship {} finished collecting. Returning to base".format(si.id))
                si.state = ShipState.RETURN
                si.target = self.nearest_dropoff_prioritize(si.ship.position)
            elif halite_remaining < self.parameters.ship_stop_collecting(self.game.turn_number):
                logging.info("Ship {} finished collecting. Choosing new target".format(si.id))
                si.state = ShipState.MOVE
                ships_require_targets.append(si)

        # Return
        for si in state_shipids[ShipState.RETURN]:
            if si.ship.position == si.target:
                logging.info("Ship {} dropped off cargo. Choosing new target".format(si.id))
                si.state = ShipState.MOVE
                ships_require_targets.append(si)
                self.stats.ship_ended_run(si.id)

        # Reposition blockade
        blockade_targets = defaultdict(list)
        for si in state_shipids[ShipState.BLOCKADE]:
            blockade_targets[si.blockade_target].append(si)
        for blockade_target, ships in blockade_targets.items():
            distance_to_blockade_target = {si : self.game_map.calculate_distance(si.ship.position, blockade_target) for si in ships}
            secured_ships = []
            remaining_ships = []
            for si in ships:
                if distance_to_blockade_target[si] == 1 and si.ship.position != blockade_target:
                    secured_ships.append(si)
                else:
                    remaining_ships.append(si)
            cross_targets_secured = set([si.ship.position for si in secured_ships])
            cross_targets_unsecured = set(blockade_target.get_surrounding_cardinals()) - cross_targets_secured

            remaining_ships.sort(key=lambda x : distance_to_blockade_target[x])
            for si in remaining_ships:
                target = blockade_target
                if cross_targets_unsecured:
                    target = min(cross_targets_unsecured, key = lambda x : self.game_map.calculate_distance(si.ship.position, x))
                    cross_targets_unsecured.remove(target)
                si.target = target

        # Building dropoffs
        if ships_require_targets and self.want_to_build_dropoff() and self.going_to_build_dropoffs() == 0:
            best_positions = self.get_best_dropoff_positions()
            if best_positions:
                i,j = best_positions[0]
                best_dropoff_pos = Position(i,j)

                dists = {si : self.game_map.calculate_distance(si.ship.position, best_dropoff_pos) for si in ships_require_targets}
                res = min(dists, key=lambda k : dists[k])
                if dists[res] <= self.parameters.dropoff_max_move_distance:
                    logging.info("Moving to build dropoff {} {}".format(res.id, dists[res]))
                    res.state = ShipState.DROPOFF
                    res.target = best_dropoff_pos
                    ships_require_targets = [si for si in ships_require_targets if si.id != res.id]

        # Quick return
        if len(self.ship_info) <= 5 and self.game.turn_number < 80 and self.early_return:
            cost_to_shipyard = { si : self.game_map.get_path_cost(si.ship.position, self.me.shipyard.position) for si in self.ship_info.values() if si.state != ShipState.RETURN}
            return_to_shipyard = { si : si.ship.halite_amount - cost for si, cost in cost_to_shipyard.items() }
            ships_positive_return = [ si for si, return_amount in return_to_shipyard.items() if return_amount > 0 ]
            moves_to_shipyard = { si : len(self.game_map.get_path(si.ship.position, self.me.shipyard.position)) - 1 for si in ships_positive_return }
            possible_pairs = {}
            for si0, si1 in permutations(ships_positive_return, 2):
                total_return = return_to_shipyard[si0] + return_to_shipyard[si1]
                total_time = max(moves_to_shipyard[si0], moves_to_shipyard[si1])
                total_cost = cost_to_shipyard[si0] + cost_to_shipyard[si1]
                if moves_to_shipyard[si0] == moves_to_shipyard[si1]:
                    total_time += 1
                if total_return >= constants.SHIP_COST and total_cost < 400:
                    possible_pairs[(si0, si1)] = (total_time, total_cost)
            for k, v in possible_pairs.items():
                logging.info("possible_pairs {} : {}".format(k, v))
            if possible_pairs:
                res = min(possible_pairs, key=lambda x : possible_pairs[x])
                for si in res:
                    logging.info("Ship {} early return. Returning to base".format(si.id))
                    si.state = ShipState.RETURN
                    si.target = self.nearest_dropoff_prioritize(si.ship.position)
                self.early_return = False

        # New Targets
        targets = {}
        for si in ships_require_targets:
            possible_targets = self.get_n_targets(si.ship.position, len(ships_require_targets))
            for t, dist in possible_targets:
                targets[(t, si.id)] = dist

        for _ in range(len(ships_require_targets)):
            t, ship_id = min(targets, key=lambda k : targets[k])
            self.ship_info[ship_id].target = t
            self.stats.ship_started_run(ship_id)
            logging.info("Ship {} new target {} dist {}".format(ship_id, t, targets[(t, ship_id)]))
            targets = {k : v for k, v in targets.items() if k[0] != t and k[1] != ship_id}


    def ship_moves_to_commands(self, ship_moves):
        command_queue = []
        for ship_move in ship_moves:
            ship = self.ship_info[ship_move.ship_id].ship
            command_queue.append(ship.move(ship_move.direction))
        return command_queue

    def calculate_next_actions(self):
        command_queue = []

        ship_moves, commands = self.get_moves()
        if commands:
            command_queue.extend(commands)
        self.collision_change_moves(ship_moves)

        command_queue.extend(self.ship_moves_to_commands(ship_moves))

        if self.spawn_ship(ship_moves):
            logging.info("Spawning ship")
            command_queue.append(self.me.shipyard.spawn())

        return command_queue

    def spawn_ship(self, ship_moves):
        if self.game.turn_number <= self.parameters.max_spawn_turn:
            all_end_pos = set([sm.end for sm in ship_moves])
            return self.me.halite_amount >= (constants.SHIP_COST + self.going_to_build_dropoffs() * constants.DROPOFF_COST) and self.me.shipyard.position not in all_end_pos

    def get_current_targets(self):
        return [si.target for si in self.ship_info.values() if (si.state == ShipState.COLLECTING or si.state == ShipState.MOVE) and si.target is not None]

    def get_n_targets(self, position, n):
        x = [(p.x, p.y) for p in self.get_current_targets()]
        current_targets = set(x)

        distances = self.game_map.get_distances(position)
        scores = self.game_map.halite_map / distances

        constuction_dropoff = self.get_constuction_dropoff_map()
        scores *= constuction_dropoff

        # logging.info("Multiple by inspire {}".format(np.array2string(self.get_inspiration_map()[0:10,0:10])))
        scores *= self.get_inspiration_map()
        ind = np.unravel_index(np.argsort(scores, axis=None), scores.shape)
        coords = list(reversed(list(zip(ind[0], ind[1]))))

        valid_coords = [c for c in coords if c not in current_targets]

        return [(Position(i, j), distances[i,j]) for i,j in valid_coords[:n]]

    def get_target(self, position):
        return self.get_n_targets(position, 1)[0][0]

    def get_moves(self):
        ship_moves = []
        commands = []

        state_shipids = defaultdict(list)
        for si in self.ship_info.values():
            state_shipids[si.state].append(si)

        for si in state_shipids[ShipState.DROPOFF]:
            if si.target == si.ship.position:
                ha = self.me.halite_amount
                cost = constants.DROPOFF_COST - si.ship.halite_amount - self.game_map.halite_amount(si.ship.position)
                if ha >= cost:
                    dropoff_position = si.ship.position
                    logging.info("Created dropoff as {}".format(dropoff_position))
                    commands.append(si.ship.make_dropoff())

                    for si in state_shipids[ShipState.RETURN]:
                        d1 = self.game_map.calculate_distance(si.ship.position, si.target)
                        d2 = self.game_map.calculate_distance(si.ship.position, dropoff_position)
                        if d2 - d1 <= self.parameters.dropoff_return_dist:
                            logging.info("Ship {} changed target to new dropoff {}".format(si.id, dropoff_position))
                            si.target = dropoff_position

                else:
                    logging.info("Reached dropoff point - can't build")
                    ship_moves.append(ShipMove.from_si(si, Direction.Still, self.game_map.calculate_distance(si.ship.position, si.target)))
            else:
                ship_moves.append(self.get_move(si))

        for si in state_shipids[ShipState.COLLECTING]:
            ship_moves.append(ShipMove.from_si(si, Direction.Still, self.game_map.calculate_distance(si.ship.position, si.target)))

        for state in [ShipState.MOVE, ShipState.RETURN, ShipState.RETURN_END, ShipState.BLOCKADE]:
            for si in state_shipids[state]:
                ship_moves.append(self.get_move(si))

        return ship_moves, commands

    def get_move(self, si):
        move = Direction.Still
        if si.can_move:
            # logging.info("Ship {} Get move from {} to {}".format(si.id, si.ship.position, si.target))
            res = self.game_map.get_path(si.ship.position, si.target)
            if res:
                move = res[0]
                next_pos = si.ship.position.directional_offset(move)
                # logging.info("Ship {} moving to {}".format(si.id, next_pos))
        else:
            logging.info("Ship {} stuck at {}".format(si.id, si.ship.position))

        return ShipMove.from_si(si, move, self.game_map.calculate_distance(si.ship.position, si.target))

    def collision_change_moves(self, ship_moves):
        ship_moves.sort(key=lambda x : [x.priority])
        if self.game.turn_number < 10:
            for sm0, sm1 in permutations(ship_moves, 2):
                if sm0.end == sm1.start and sm1.end != sm0.start:
                    si0 = self.ship_info[sm0.ship_id]
                    si1 = self.ship_info[sm1.ship_id]
                    if si0.state == ShipState.MOVE and si1.state == ShipState.COLLECTING and si0.can_move and si1.can_move:
                        s0 = si0.ship
                        s1 = si1.ship
                        logging.info("Swap targets Moving {} Collecting {}".format(sm0.ship_id, sm1.ship_id))
                        logging.info("{} pos {} target {} state {}".format(sm0.ship_id, s0.position, si0.target, si0.state))
                        logging.info("{} pos {} target {} state {}".format(sm1.ship_id, s1.position, si1.target, si1.state))
                        si0.target, si1.target = si1.target, si0.target
                        si1.state = ShipState.MOVE

                        res = self.game_map.get_path(si1.ship.position, si1.target)
                        if res:
                            move = res[0]
                            next_pos = si1.ship.position.directional_offset(move)
                            logging.info("Ship {} moving to {}".format(si1.id, next_pos))

                        sm1.set_new_direction(move)
                        sm1.priority = si1.priority()

        ship_moves = fix_collisions(ship_moves)
        
    def update_ship_info(self):
        all_ship_ids = set([s.id for s in self.ships])
        ship_info_ids = set([si.id for si in self.ship_info.values()])
        new_ship_ids = all_ship_ids - ship_info_ids
        del_ship_ids = ship_info_ids - all_ship_ids
        if new_ship_ids:
            logging.info("New ships {}".format(new_ship_ids))
            new_ships = [s for s in self.ships if s.id in new_ship_ids]
            for ship in new_ships:
                self.ship_info[ship.id] = ShipInfo(ship)
        if del_ship_ids:
            logging.info("Removed ships {}".format(del_ship_ids))
            self.ship_info = {si.id : si for si in self.ship_info.values() if si.id not in del_ship_ids}
        for si in self.ship_info.values():
            si.can_move = self.can_ship_move(si.ship)
            # logging.info("Ship {} at {} - H:{} S:{} HoM:{} Can Move:{}".format(si.id, si.ship.position, si.ship.halite_amount, si.state, self.game_map.halite_amount(si.ship.position), si.can_move))

    def can_ship_move(self, ship):
        cost = self.game_map.cost_to_move(ship.position)
        return cost <= ship.halite_amount

    def turns_remaining(self):
        return constants.MAX_TURNS - self.game.turn_number

    def distance_to_dropoff(self, position):
        return self.game_map.calculate_distance(self.nearest_dropoff(position), position)

    def get_enemy_players(self):
        return [player for i, player in self.game.players.items() if i != self.me.id]

    def get_enemy_ships(self):
        ships = []
        for player in self.get_enemy_players():
            ships.extend(player.get_ships())
        return ships

    def get_enemy_ship_map(self):
        ships = np.zeros((self.game_map.size(), self.game_map.size()), dtype=int)
        for ship in self.get_enemy_ships():
            p = ship.position
            ships[p.x, p.y] = 1
        return ships

    def get_inspiration_map(self):
        enemy_ships = self.get_enemy_ship_map()
        m = ndimage.convolve(enemy_ships, INSPIRATION_CONV, mode='wrap')
        m[m < 2] = 1
        m[m > 1] = self.parameters.inspire_target_bonus
        return m

    def get_constuction_dropoff_map(self):
        dropoffs = np.zeros((self.game_map.size(), self.game_map.size()))
        for si in self.ship_info.values():
            if si.state == ShipState.DROPOFF:
                p = si.target
                dropoffs[p.x, p.y] = 1
        dropoffs = ndimage.convolve(dropoffs, CONSTRUCTION_DROPOFF_CONV, mode='wrap')
        dropoffs *= 0.2
        dropoffs += 1
        return dropoffs


    def get_dropoffs(self):
        return self.me.get_dropoffs()

    def get_base_positions(self):
        dropoff_pos = [self.me.shipyard.position]
        for d in self.get_dropoffs():
            dropoff_pos.append(d.position)
        return dropoff_pos

    def get_all_base_positions(self):
        positions = []
        for player in self.game.players.values():
            positions.append(player.shipyard.position)
            for d in player.get_dropoffs():
                positions.append(d.position)
        return positions

    def get_enemy_base_positions(self):
        positions = []
        for player in self.game.players.values():
            if player.id != self.me.id:
                positions.append(player.shipyard.position)
                for d in player.get_dropoffs():
                    positions.append(d.position)
        return positions

    def going_to_build_dropoffs(self):
        return len([si for si in self.ship_info.values() if si.state == ShipState.DROPOFF])

    def want_to_build_dropoff(self):
        return (len(self.get_dropoffs()) + self.going_to_build_dropoffs()) < self.parameters.max_dropoffs and self.game.turn_number >= self.parameters.dropoff_min_time

    def nearest_dropoff(self, pos):
        return min(self.get_base_positions(), key=lambda x : self.game_map.calculate_distance(pos, x))

    def nearest_dropoff_prioritize(self, pos):
        base_positions = self.get_base_positions()
        dropoffs = set(base_positions[1:])
        return min(base_positions, key=lambda x : self.game_map.calculate_distance(pos, x) - (self.parameters.dropoff_return_priority if x in dropoffs else 0))

    def get_best_dropoff_positions(self):
        scores = ndimage.convolve(self.game_map.halite_map, DROPOFF_CONV, mode='wrap')
        halite_cutoff = self.parameters.dropoff_halite_min * DROPOFF_CONV_SUM
        scores[scores < halite_cutoff] = 0

        if np.max(scores) < halite_cutoff:
            return []
        
        distances = self.game_map.get_distances(self.me.shipyard.position)

        for p in self.get_base_positions():
            d = self.game_map.get_distances(p)
            distances = np.minimum(distances, d)

        distances[distances < self.parameters.dropoff_min_distance] = 0
        distances[distances > self.parameters.dropoff_max_distance] = 0
        distances[distances > 0] = 1

        scores *= distances
        
        ind = np.unravel_index(np.argsort(scores, axis=None), scores.shape)
        existing_dropoffs = set([(p.x, p.y) for p in self.get_all_base_positions()])
        return [c for c in reversed(list(zip(ind[0], ind[1]))) if c not in existing_dropoffs]

    def get_enemy_cross_positions(self):
        cross_positions = set()
        for pos in self.get_enemy_base_positions():
            cross_positions.update(pos.get_surrounding_cardinals())
        return list(cross_positions)

    def get_enemy_ships(self):
        ships = []
        for player in self.game.players.values():
            if player.id != self.me.id:
                for ship in player.get_ships():
                    ships.append(ship)
        return ships
