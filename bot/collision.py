from hlt.positionals import Direction
from collections import Counter

def get_locked_moves(moves):
    return [move for move in moves if move.priority <= 1 or move.no_collision_check]

def get_remaining_moves(moves, locked_moves):
    return [move for move in moves if move not in locked_moves]

def get_dependencies(move, moves):
    moves_except = [m for m in moves if move.ship_id != m.ship_id]
    mm = [m for m in moves_except if move.start == m.end]
    if not mm:
        return 0
    deps = []
    for m in mm:
        dep = get_dependencies(m, moves_except) + 1
        deps.append(dep)
    return max(deps)

def fix_collisions(moves):
    locked_moves = get_locked_moves(moves)
    locked_end_pos = set([move.end for move in locked_moves])
    remaining_moves = get_remaining_moves(moves, locked_moves)
    while True:
        remaining_pos_count = Counter([move.end for move in remaining_moves])
        colliding_moves = {move : get_dependencies(move, remaining_moves) for move in remaining_moves if move.end in locked_end_pos or remaining_pos_count[move.end] > 1}
        if not colliding_moves:
            break
        move = min(colliding_moves, key=(lambda move: colliding_moves[move]))
        move.end = move.start
        move.direction = Direction.Still
