from enum import Enum

class ShipState(Enum):
    START = 0
    MOVE = 1
    COLLECTING = 2
    RETURN = 3
    RETURN_END = 4
    DROPOFF = 5
    BLOCKADE = 6

state_priority = {
    ShipState.COLLECTING : 1,
    ShipState.RETURN : 2,
    ShipState.RETURN_END : 2,
    ShipState.DROPOFF : 3,
    ShipState.MOVE : 4,
    ShipState.BLOCKADE : 5
}

class ShipInfo():
    def __init__(self, ship):
        self.ship = ship
        self.id = ship.id
        self.target = None
        self.state = ShipState.START
        self.can_move = True
        self.blockage_target = None

    def priority(self):
        if not self.can_move:
            return 0
        
        return state_priority[self.state]

    def __hash__(self):
        return hash(self.id)