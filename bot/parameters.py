values = {
    2 : {
        32 : [230, 60, 0.10, 850, 40, 24, 100, 8, 12, 10, 1, 4, 4, 200, 1, 60],
        40 : [244, 60, 0.10, 850, 40, 24, 100, 8, 12, 10, 1, 4, 0, 200, 1, 120],
        48 : [258, 60, 0.10, 850, 40, 24, 100, 8, 12, 10, 1, 4, 0, 200, 1, 100],
        56 : [273, 60, 0.10, 850, 40, 28, 100, 8, 14, 10, 1, 4, 0, 200, 1, 20],
        64 : [287, 60, 0.10, 850, 40, 24, 100, 8, 16, 10, 2, 4, 0, 200, 1, 20]
    },
    4 : {
        32 : [162, 60, 0.10, 910, 65, 25, 192, 5, 4, 13, 0, 4, 0, 1000, 1, 60],
        40 : [220, 60, 0.10, 890, 18, 28, 100, 8, 20, 10, 1, 4, 0, 200, 1, 60],
        48 : [220, 60, 0.10, 850, 12, 24, 100, 8, 20, 10, 1, 4, 0, 200, 1, 40],
        56 : [220, 60, 0.10, 850, 12, 28, 100, 8, 20, 10, 1, 4, 0, 200, 1, 20],
        64 : [220, 60, 0.10, 850, 12, 28, 100, 8, 20, 10, 2, 4, 0, 200, 1, 20]
    }
}

class Parameters(object):

    def __init__(self, max_spawn_turn, ship_stop_collecting_const, ship_stop_collecting_per_turn, ship_return_dropoff, move_additional_cost,
                 end_return_time, dropoff_min_time, dropoff_min_distance, dropoff_max_distance_range, dropoff_max_move_distance,
                 max_dropoffs, dropoff_return_dist, dropoff_return_priority, dropoff_halite_min, inspire_target_bonus, enemy_ship_avoid_cost):
        self.max_spawn_turn = max_spawn_turn
        self.ship_stop_collecting_const = ship_stop_collecting_const
        self.ship_stop_collecting_per_turn = ship_stop_collecting_per_turn
        self.ship_return_dropoff = ship_return_dropoff
        self.move_additional_cost = move_additional_cost
        self.end_return_time = end_return_time

        self.dropoff_min_time = dropoff_min_time
        self.dropoff_min_distance = dropoff_min_distance
        self.dropoff_max_distance = dropoff_min_distance + dropoff_max_distance_range
        self.dropoff_max_move_distance = dropoff_max_move_distance
        self.max_dropoffs = max_dropoffs
        self.dropoff_return_dist = dropoff_return_dist
        self.dropoff_return_priority = dropoff_return_priority
        self.dropoff_halite_min = dropoff_halite_min

        self.inspire_target_bonus = inspire_target_bonus

        self.enemy_ship_avoid_cost = enemy_ship_avoid_cost

    def ship_stop_collecting(self, turn_number):
        return max(self.ship_stop_collecting_const + turn_number * -self.ship_stop_collecting_per_turn, 0)

    @classmethod
    def from_game(cls, game):
        size = game.game_map.width
        total_players = len(game.players)
        return cls(*values[total_players][size])

    @classmethod
    def from_array(cls, a):
        return cls(*list(map(float, a.split(','))))
