import hlt
from bot.gamestate import GameState
from bot.parameters import Parameters
import logging

class GameController(object):

    def __init__(self, game, parameters):
        self.game_state = GameState(game, parameters)

    @classmethod
    def from_game(cls, game, args):
        if len(args) == 2:
            logging.info(args)
            parameters = Parameters.from_array(args[1])
        else:
            parameters = Parameters.from_game(game)
        return cls(game, parameters)

    def get_commands(self, game):
        self.game_state.update(game)

        logging.info("--- Completed actions ---")
        self.game_state.update_completed_actions()

        logging.info("--- Calculate next actions ---")
        command_queue = self.game_state.calculate_next_actions()

        return command_queue
