from MatchRunner import Player, MatchRunner, SIZES
from CollectHalite import Stats
import numpy as np
import argparse

np.random.seed(0)

def duel(size, games, progress, player, four_players):
    print(player)

    if four_players:
        enemies = [Player.enemy(0), Player.enemy(0), Player.enemy(0)]
    else:
        enemies = [Player.enemy(0)]
    
    mr = MatchRunner(size)
    mr.add_player(player)
    for e in enemies:
        mr.add_player(e)

    results = mr.match(n=games, seed_val=1, progress=progress)

    all_stats = [Stats.from_results(results, size, j) for j in range(results[0].total_players())]
    for stats in all_stats:
        stats.logline()
    halite_collected = all_stats[0].halite_collected
    enemy_halite_collected = max(stats.halite_collected for stats in all_stats[1:])
    fitness = halite_collected - enemy_halite_collected

    print(fitness)

    return fitness

def main(size, all_sizes, games, progress, four_players):

    # p0 = Player.default()
    p0 = Player.from_bot_parameters(4 if four_players else 2, size)
    # p0 = Player.default_para([244, 10, 0, 850, 40, 24, 100, 8, 12, 10, 1, 4, 0, 200, 1])
    # p1 = Player.default_para([244, 10, 0, 850, 40, 28, 100, 8, 12, 10, 1, 4, 0, 200, 1])
    # p2 = Player.default_para([244, 10, 0, 850, 40, 32, 100, 8, 12, 10, 1, 4, 0, 200, 1])


    players = []
    players.append(p0)
    # players.append(p1)
    # players.append(p2)
    # players.append(p3)
    # players.append(p4)

    for p in players:
        if all_sizes:
            for s in SIZES:
                duel(s, games, progress, p, four_players)
        else:
            duel(size, games, progress, p, four_players)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', '-s', action="store", default=32, type=int)
    parser.add_argument('--games', '-g', action="store", default=10, type=int)
    parser.add_argument('--progress', '-p', action='store_true')
    parser.add_argument('--four', '-4', action='store_true')
    parser.add_argument('--all_sizes', '-a', action='store_true')
    args = parser.parse_args()
    main(args.size, args.all_sizes, args.games, args.progress, args.four)
